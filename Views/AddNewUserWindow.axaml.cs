using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using lab3.Models.Repositories;
using lab3.ViewModels;

namespace lab3.Views
{
    public partial class AddNewUserWindow : Window
    {
        public AddNewUserWindow()
        {
            InitializeComponent();
            DataContext = new AddNewUserViewModel(this, new JsonPersonRepository());
#if DEBUG
            this.AttachDevTools();
#endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
