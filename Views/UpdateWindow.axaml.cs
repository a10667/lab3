using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using lab3.Models.Repositories;
using lab3.ViewModels;

namespace lab3.Views
{
    public partial class UpdateWindow : Window
    {
        public UpdateWindow()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
        }
        public UpdateWindow(Models.IPerson selectedPerson)
        {
            InitializeComponent();
            DataContext = new UpdateUserViewModel(this, selectedPerson, new JsonPersonRepository());
#if DEBUG
            this.AttachDevTools();
#endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
