using Avalonia.Controls;
using Avalonia.Interactivity;
using lab3.ViewModels;

namespace lab3.Views
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void DeleteButtonClick(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as MainWindowViewModel;
            vm.DeletePerson.Execute(vm.SelectedPerson);

        }
        private void UpdateButtonClick(object sender, RoutedEventArgs e)
        { 
            var vm = DataContext as MainWindowViewModel;
            vm.UpdatePerson.Execute(vm.SelectedPerson);
        }
    }
}