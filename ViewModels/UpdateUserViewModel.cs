﻿using lab3.Models;
using lab3.Models.Repositories;
using lab3.Views;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace lab3.ViewModels
{
    public class UpdateUserViewModel : ViewModelBase
    {
        private IPerson _currentUser;
        private ICommand _updateUserCommand;
        private int _id;
        private string _lastname;
        private string _secondname;
        private string _name;
        private ReactiveCommand<Unit, Unit> _backCommand;
        private readonly IPersonRepository _personRepositroy;

        private readonly UpdateWindow _view;

        public UpdateUserViewModel(UpdateWindow view, IPerson currentUser, IPersonRepository personRepositroy)
        {
            _currentUser = currentUser;
            this._personRepositroy = personRepositroy;
            Name = currentUser.Name;
            SecondName = currentUser.SecondName;
            LastName = currentUser.LastName;
            _view = view;
        }

        public string Name { get => _name; set => this.RaiseAndSetIfChanged(ref _name, value); }
        public string SecondName { get => _secondname; set => this.RaiseAndSetIfChanged(ref _secondname, value); }
        public string LastName { get => _lastname; set => this.RaiseAndSetIfChanged(ref _lastname, value); }

        public ICommand UpdateUserCommand => _updateUserCommand ??= ReactiveCommand.Create(() => {
            var updated = Person.Copy(_currentUser);
            updated.Name = Name;
            updated.LastName = LastName;
            updated.SecondName = SecondName;
            _personRepositroy.Update(_currentUser, updated);
            MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Title", "Person was Updateds").ShowDialog(_view);

        });

        public ICommand Back => _backCommand ??= ReactiveCommand.Create(() => {
            _view.Close();
        });
    }
}
