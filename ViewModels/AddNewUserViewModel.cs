﻿using Avalonia.Controls;
using lab3.Models;
using lab3.Models.Repositories;
using lab3.Views;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace lab3.ViewModels
{
    public class AddNewUserViewModel : ViewModelBase
    {
        private ICommand _addUserCommand;
        private string _lastname;
        private string _secondname;
        private string _name;
        private ReactiveCommand<Unit, Unit> _backCommand;
        private int _id;
        private readonly IPersonRepository _personRepositroy;

        private readonly Window _view;

        public AddNewUserViewModel(AddNewUserWindow view, IPersonRepository personRepositroy)
        {
            this._personRepositroy = personRepositroy;
            _view = view;
            GenidAsync();
        }

        private async Task GenidAsync()
        {
            _id = await NewPersonIdAsync();
        }

        private async Task<int> NewPersonIdAsync()
        {
            return (await Task.Run(() => _personRepositroy.GetAll().Count())) + 1;
        }

        public string Name { get => _name; set => this.RaiseAndSetIfChanged(ref _name, value); }
        public string SecondName { get => _secondname; set => this.RaiseAndSetIfChanged(ref _secondname, value); }
        public string LastName { get => _lastname; set => this.RaiseAndSetIfChanged(ref _lastname, value); }

        public ICommand AddUser => _addUserCommand ??= ReactiveCommand.Create(() => {
            _personRepositroy.Add(new Person (_id, Name, SecondName, LastName, null));
            MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Title", "Person was Added").ShowDialog(_view);
        });

        public ICommand Back => _backCommand ??= ReactiveCommand.Create(() => {
            _view.Close();
        });
    }
}