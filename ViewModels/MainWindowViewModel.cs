﻿using lab3.Models;
using lab3.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using ReactiveUI;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using lab3.Views;
using Avalonia.Controls;
using System.Reactive;
using System.Reactive.Linq;

namespace lab3.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly Window view;
        private readonly IPersonRepository _personRepository;

        private IPerson _selectedPerson;
        private ObservableCollection<IPerson> _people;
        private ICommand _addNewPerson;
        private ReactiveCommand<IPerson, Unit> _updatePerson;
        private ReactiveCommand<IPerson, Unit> _deletePerson;
        private ReactiveCommand<Unit, Unit> _back;

        public MainWindowViewModel() 
        {
            People = new ObservableCollection<IPerson>(new[] {new Person(1, "adasd", "dasdas", "asdasd", null)});
        }
        
        public MainWindowViewModel(Window view, IPersonRepository personRepository)
        {
            this.view = view;
            _personRepository = personRepository;
            _ = UpdatePeopleAsync();
        }

        private async Task UpdatePeopleAsync()
        {
            _ = await Task.Run(() => People = new ObservableCollection<IPerson>(_personRepository.GetAll()));
        }

        public ObservableCollection<IPerson> People { get => _people; set => this.RaiseAndSetIfChanged(ref _people, value); }
        public IPerson SelectedPerson { get => _selectedPerson; set => this.RaiseAndSetIfChanged(ref _selectedPerson, value); }

        public ICommand AddNewPerson => _addNewPerson ??= ReactiveCommand.Create(() => {

            var v = new AddNewUserWindow();
            v.Closed += (s, e) => UpdatePeopleAsync();
            v.ShowDialog(view);
        });



        public ICommand DeletePerson => _deletePerson ??= ReactiveCommand.Create<IPerson>(p =>
        {
            _personRepository.Remove(SelectedPerson);
            _ = UpdatePeopleAsync();
            MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Title", "Person was Deleted").ShowDialog(view);
        });

        public ICommand UpdatePerson => _updatePerson ??= ReactiveCommand.Create<IPerson>(async p => {
            var v = new UpdateWindow(SelectedPerson);
            v.Closed += (s, e) => UpdatePeopleAsync();
            v.ShowDialog(view);
        });

        public ICommand Back => _back ??= ReactiveCommand.Create(() => {
            view.Close();
        });

    }
}
