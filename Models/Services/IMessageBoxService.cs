﻿namespace lab3.Models.Services;

public interface IMessageBoxService
{
    void Show(string message);
    void ShowError(string message);
}