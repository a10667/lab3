﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3.Models.Repositories
{
    public interface IPersonRepository
    {
        IEnumerable<IPerson> GetAll();
        IPerson First(Func<IPerson, bool> predicate);
        IPersonRepository Add(IPerson person);
        IPersonRepository Remove(IPerson person);
        IPersonRepository Update(IPerson old, IPerson nnew);
    }
}
