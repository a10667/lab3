﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3.Models.Repositories
{
    internal class JsonPersonRepository : IPersonRepository
    {
        private List<IPerson> people = new List<IPerson>();

        public JsonPersonRepository()
        {

            if (!File.Exists("database.json"))
                File.Create("database.json");

            var res = GetAll();
            if (res is not null)
            people = new List<IPerson>(res);
        }
        public IPersonRepository Add(IPerson person)
        {
            people.Add(person);
            SaveChanges();

            return this;
        }

        public IPerson First(Func<IPerson, bool> predicate)
        {
            return people.First(predicate);
        }

        public IEnumerable<IPerson> GetAll()
        {
            var file = File.ReadAllText("database.json");
            var data=  JsonConvert.DeserializeObject<IEnumerable<Person>>(file);
            if (data is not null)
                people = new List<IPerson>(data);
               
            return people;
        }

        public IPersonRepository Remove(IPerson person)
        {
            people.Remove(person);
            SaveChanges();

            return this;
        }

        public IPersonRepository Update(IPerson old, IPerson nnew)
        {

            var o = people.First(p => p.Id == old.Id);
            o.LastName = nnew.LastName;
            o.SecondName = nnew.SecondName;
            o.Name = nnew.Name;

            SaveChanges();
            return this;
        }

        private void SaveChanges()
        {
            var result = JsonConvert.SerializeObject(people);
            File.WriteAllText("database.json", result);
        }
    }
}
