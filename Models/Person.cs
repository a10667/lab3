﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3.Models
{
    internal class Person : IPerson
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Person(int id, string name, string secondName, string lastName, byte[] photo)
        {
            Name = name;
            SecondName = secondName;
            LastName = lastName;
            Photo = photo;
            Id = id;
        }

        public string SecondName { get; set; }

        public string LastName { get; set; }

        public byte[] Photo { get; set; }

        public static IPerson Copy(IPerson person) => new Person(person.Id, person.Name, person.SecondName, person.LastName, person.Photo);
    }
}
